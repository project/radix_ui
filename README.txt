Radix UI
=============

This module provides Bootstrap UI elements for the Radix theme for Panopoly.
Note that you need not use panopoly nor radix to make this work, in fact, you
just need to be using twitter bootstrap with your theme. While there may
occasionally be some things which are radix and panopoly specific, these should 
generally be useful across the board.

Basic Requirements:
-------------------

* Twitter Bootstrap 2.x (css via a theme)
* bootstrap_api - a module which allows other modules to load bootstrap jquery component libraries contextually!
* jQuery Update (You want to at least jQuery 1.8 or higher on the frontend)

Recommended Modules/Themes:
---------------------------

* Panels (Style them up! The bulk of this module will be ctools style plugins)
* Panopoly
* Radix Theme and friends
* Views (plugins on the way)

So far:

Field Formatters
----------------

* Progress Indicator for various numerical fields. 
This is handy for say, kickstarter style widgets where commerce + rules are 
generating a numerical value *hint hint*.

Panel Style Plugins
-------------------

Regions:

* Well
* Tabbable (tabs/pills)
* Accordion
* Collapse

Pane Styles:

* Well

Coming Soon To a Repo Near You:
-------------------------------

* Navs galore - tabs, menu style plugins, etc. for ctools
* Spotlight Widget overhauled for Bootstrap Carousel
* Accordions, Collapse Buttons for Panes, etc.
* Some kinda ctools magic with Fontawesome icons?
