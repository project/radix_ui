<?php
/**
 * @file
 * The tabbable style plugin that is part of Radix UI
 */

// Plugin definition.
$plugin = array(
  'title' => t('Radix Tabbable Style'),
  'description' => t('Display panes as tabs, bootstrap style'),
  'render region' => 'radix_ui_tabbable_render_region',
  'settings form' => 'radix_ui_tabbable_style_settings_form',
  'weight' => -15,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_radix_ui_tabbable_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];
  drupal_add_library('bootstrap_api', 'bootstrap-tab');

  if (empty($display->css_id)) {
    $region_id = 'tabbable-panels-' . $region_id;
  }
  else {
    $region_id = 'tabbable-' . $display->css_id;
  }

  // Display the Output.
  $output = '<ul class="nav nav-tabs nav-' . $settings['navs_style'] . '" id="' . $region_id . '">';
  $items = array();
  $delta_tabs = 1;
  foreach ($panes as $pane_id => $pane) {
    if ($pane_id != 'empty_placeholder') {
      $title = radix_ui_pane_titles($display->did, $pane_id);
      $title = $title ? $title : t('Tab @delta', array('@delta' => $delta_tabs));
      if ($delta_tabs == $settings['navs_active']) {
        $output .= '<li class="tabbable-tab active"> <a href="#' . $pane_id . '" data-toggle="tab" >' . $title . '</a></li>';
      }
      else {
        $output .= '<li class="tabbable-tab"> <a href="#' . $pane_id . '" data-toggle="tab" >' . $title . '</a></li>';
      }
      ++$delta_tabs;
    }
  }
  $output .= '</ul>';
  $output .= '<div class="tab-content" id="' . $region_id . '">';

  // What a hack!!  Dont show your boss this!
  $delta_panes = 1;
  foreach ($panes as $pane_id => $pane) {
    if ($pane_id != 'empty_placeholder') {
      if ($delta_panes == $settings['navs_active']) {
        $output .= '<div class="tab-pane active ' . $region_id . '-' . $pane_id . '" id="' . $pane_id . '">' . $pane . '</div>';
      }
      else {
        $output .= '<div class="tab-pane ' . $region_id . '-' . $pane_id . '" id="' . $pane_id . '">' . $pane . '</div>';
      }
      ++$delta_panes;
    }
  }
  $output .= '</div>';

  // Handle the Empty Placeholder Panels IPE Needs.
  if (!empty($panes['empty_placeholder'])) {
    $output .= $panes['empty_placeholder'];
  }

  return $output;
}

/**
 * Settings form callback.
 */
function radix_ui_tabbable_style_settings_form($style_settings) {
  $form['navs_style'] = array(
    '#type' => 'select',
    '#title' => t('Tabs or Pills?'),
    '#options' => array('tabs' => t('Tabs'), 'pills' => t('Pills')),
    '#default_value' => (isset($style_settings['navs_style'])) ? $style_settings['navs_style'] : 'tab',
  );
  $form['navs_active'] = array(
    '#type' => 'textfield',
    '#title' => t('Which tab is active'),
    '#default_value' => (isset($style_settings['navs_active'])) ? $style_settings['navs_active'] : '1',
  );
  return $form;
};
