<?php

/**
 * @file
 * The collapse plugin that is part of Radix UI
 */

// Plugin definition.
$plugin = array(
  'title' => t('Radix Collapse Toggle'),
  'description' => t('Display a region as a collapse toggle'),
  'render region' => 'radix_ui_collapse_style_render_region',
  'settings form' => 'radix_ui_collapse_style_settings_form',
  'weight' => -15,
);

/**
 * Implements hook_render_region().
 *
 * Renders the collapse region.
 */
function theme_radix_ui_collapse_style_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];
  drupal_add_library('bootstrap_api', 'bootstrap-api.collapse');

  $output = '';
  $output .= '<div class="region region-' . $vars['region_id'] . '">';
  if ($settings['collapse_on'] == 0) {
    $output .= '<div class="region-inner collapse in" id="collapse-' . $display_id . '">';
  }
  else {
    $output .= '<div class="region-inner collapse" id="collapse-' . $display_id . '">';
  }
  $output .= implode('<div class="panel-separator"></div>', $panes);
  $output .= '</div></div>';
  $output .= '<div><button type="button" class="btn" data-toggle="collapse" data-target="#collapse-' . $display_id . '"><i class="icon-' . $settings['collapse_icon'] . '"></i> ' . $settings['collapse_text'] . '</button></div>';

  return $output;
}

/**
 * Implements hook_form().
 *
 * Builds the collapse pane/region form.
 */
function radix_ui_collapse_style_settings_form($style_settings) {
  $form['collapse_on'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default State'),
    '#description' => t('Should the element be collapsed by default?'),
    '#default_value' => (isset($style_settings['collapse_on'])) ? $style_settings['collapse_on'] : '0',
  );
  // This will need to get more elegant when a render_pane() is implemented.
  $form['collapse_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Collapse Text'),
    '#description' => t('The text on the toggle, if any'),
    '#default_value' => (isset($style_settings['collapse_text'])) ? $style_settings['collapse_text'] : t('Toggle Me!'),
  );
  $form['collapse_icon'] = array(
    '#type' => 'textfield',
    '#length' => 12,
    '#title' => t('Collapse Icon'),
    '#prefix' => 'icon-',
    '#description' => t('The icon that is used for the toggle, such as <i>arrow</i>'),
    '#default_value' => (isset($style_settings['collapse_icon'])) ? $style_settings['collapse_icon'] : 'arrow-up',
  );

  return $form;
}
