<?php
/**
 * @file
 * The accordion style plugin that is part of Radx UI
 */

// Plugin definition.
$plugin = array(
  'title' => t('Radix Accordion Style'),
  'description' => t('Display panes as accordion items, bootstrap style'),
  'render region' => 'radix_ui_accordion_style_render_region',
  'settings form' => 'radix_ui_accordion_style_settings_form',
  'weight' => -15,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_radix_ui_accordion_style_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];
  drupal_add_library('bootstrap_api', 'bootstrap-api.collapse');

  if (empty($display->css_id)) {
    $region_id = 'accordion-panels-' . $region_id;
  }
  else {
    $region_id = 'accordion-' . $display->css_id;
  }

  // Display the Output.
  $output .= '<div class="accordion" id="' . $region_id . '">';
  $items = array();
  $delta = 1;
  foreach ($panes as $pane_id => $pane) {
    if ($pane_id != 'empty_placeholder') {
      $title = radix_ui_pane_titles($display->did, $pane_id);
      $title = $title ? $title : t('Accordion Item @delta', array('@delta' => $delta));
      $output .= '<div class="accordion-group">';
      $output .= '<div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="' . $region_id . '" href="#' . $pane_id . '">' . $title . '</a> </div>';
      if ($delta == $settings['accordion_active']) {
        $output .= '<div class="accordion-body collapse in" id="' . $pane_id . '"> <div class="accordion-inner">' . $pane . '</div> </div>';
      }
      else {
        $output .= '<div class="accordion-body collapse" id="' . $pane_id . '"> <div class="accordion-inner">' . $pane . '</div> </div>';
      }
      $output .= '</div>';
      ++$delta;
    }
  }
  $output .= '</div>';

  // Handle the Empty Placeholder Panels IPE Needs.
  if (!empty($panes['empty_placeholder'])) {
    $output .= $panes['empty_placeholder'];
  }

  return $output;
};

/**
 * Settings form callback.
 */
function radix_ui_accordion_style_settings_form($style_settings) {
  $form['accordion_active'] = array(
    '#type' => 'textfield',
    '#title' => t('Which accordion item is active'),
    '#default_value' => (isset($style_settings['accordion_active'])) ? $style_settings['accordion_active'] : '1',
  );

  return $form;
};
