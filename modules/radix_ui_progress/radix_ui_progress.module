<?php
/**
 * @file
 * Formats number and text fields fields with a Twitter Bootstrap Progress Bar
 */

/**
 * Implements hook_field_formatter_info().
 */
function radix_ui_progress_field_formatter_info() {
  return array(
    'radix_ui_progress_formatter' => array(
      'label' => t('Bootstrap Progress Bar'),
      'field types' => array(
        'number_float',
        'number_decimal',
        'number_integer',
        'text',
      ),
      'settings'  => array(
        'style' => 'info',
        'active' => '1',
        'striped' => '1',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function radix_ui_progress_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  $element['style'] = array(
    '#type'           => 'select',
    '#title'          => t('Bar Style'),
    '#description'    => t('Select what style of bar it will be'),
    '#default_value'  => $settings['style'],
    '#options'        => array(
      'info'  => 'Info',
      'success' => 'Success',
      'warning'  => 'Warning',
      'danger' => 'Danger',
    ),
  );

  $element['active'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Bar is Active'),
    '#default_value'  => $settings['active'],
  );

  $element['striped'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Bar is Striped'),
    '#default_value'  => $settings['striped'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function radix_ui_progress_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = t('@style style', array(
    '@style' => $settings['style'],
  ));

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function radix_ui_progress_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];
  $style = $settings['style'];
  $striped = $settings['striped'];
  $active = $settings['active'];

  if ($striped == 1) {
    $style = $style . ' progress-striped';
  }
  if ($active == 1) {
    $style = $style . ' active';
  }

  // Output.
  foreach ($items as $delta => $item) {
    $value = $item['value'];
    $progressbar = '<div class="progress ' . 'progress-' . $style . ' ">' . '<div class="bar" style="width:' . $value . '%"></div>' . '</div>';
    $element[$delta]['#markup'] = $progressbar;
  }

  return $element;
}
